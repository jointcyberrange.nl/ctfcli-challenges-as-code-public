# ctfcli

Use the ctfcli to create and deploy CTFd challenges as code, written as YAML.
Every challenge has its own directory, with all required files for the challenge.

The ctfcli can generate templates for various kinds of challenges, these are included in the templates directory and those challenge.yaml files show what options are available for ctfcli.

## Setup ctfcli

Install the ctfcli using pip

```
pip install ctfcli
```

Setup remote access to your CTFd instance.

```
Login as CTF admin > Settings > Access Token
Set expiration date > Generate
```

Setup access to your CTFd event.

```
ctf init
Please enter CTFd instance URL: https://demo.ctfd.io
Please enter CTFd Admin Access Token: d41d8cd98f00b204e9800998ecf8427e
Do you want to continue with https://demo.ctfd.io and d41d8cd98f00b204e9800998ecf8427e [y/N]: y
Initialized empty Git repository in /Users/user/Downloads/event/.git/
```

This creates a `.ctf` directory and in it creates a `config` file.
It also initializes a local Git repository if no Git repository is found in the current directory.
The `.ctf/config` file also stores some data about challenges pushed to CTFd.

## Deploy challenges to CTFd

```
ctf challenge install [CHALLENGE_DIRECTORY e.g. cloud101]
```

Or you can iterate over all directories so all challenges are deployed to CTFd.

```
ls -1 | xargs -L 1 ctf challenge install
```

## Update challenges

If a challenge with the same name already exists, it can not be installed, but have to be synced.
The ctfcli will forcefully update challenges using the following command.

```
ctf challenge sync [CHALLENGE_DIRECTORY e.g. cloud101]
```

Or iterate over all directories so all challenges are synced.

```
ls -1 | xargs -L 1 ctf challenge sync
```

## GitLab CI

The included `.gitlab-ci.yml` installs all challenges from the repository to a CTFd event.
For this you need to supply the `CTFD_URL` & `CTFD_TOKEN` as GitLab CI variables, go to Project > Settings > CI/CD > Variables.

You also need a GitLab runner and can be enabled at Project > Settings > CI/CD > Runners. Notice in the `.gitlab-ci.yml` the tag `master`, which refers to the privately hosted runner. Be sure to include this tag for private runners, otherwise the job won't run.

### Local testing

The `CTFD_URL` needs to be publicly accessible. If you're testing on a local environment (e.g. [docker-compose.yml](https://github.com/CTFd/CTFd/blob/master/docker-compose.yml) from CTFd repo on your laptop), you can use [ngrok](https://ngrok.com/) to expose your local CTFd instance. Be sure to update the GitLab CI variable for the `CTFD_URL` every time you run ngrok.

## Next steps

- Customize the GitLab CI file to only require one per repository for every container challenge Dockerfile that sits in its own challenge directory, and only update a container image when its corresponding directory sees relevant changes.
- Automate the ctfcli workflow using GitLab CI - This examples implements a custom Python script and GitHub Actions: https://medium.com/csictf/automate-deployment-using-ci-cd-eeadd3d47ca7 or see: https://blog.ankursundara.com/uiuctf21/
- Create automated workflows for management tasks, such as marking challenges visible/hidden without editing individual challenge.yaml files and there maybe other use cases.
- Explore ctfcli options, such as image, host and healthchecks.
- Integrate with the container challenge plugin to automate connection details update and healthchecks.
- Provide custom challenge templates that include common technologjes such as Telnet, SSH, vnc and best-practices.
- Provide additional components that can be attached to challenges, such as automated tests that check if a challenge is working correctly, security scanning to validate challenges are only exploitable by your design and monitoring/logging to keep track of challenge health, progress, actions performed by players etc.
- Allow a CTF event admin to list and search different challenge repositories as sort of a catalog and include them into the event, also allow to include/exclude individual challenges.
- Evaluate ctfup for challenge deployment to Kubernetes: https://github.com/csivitu/ctfup

## Known issue

- When renaming a already existing challenge in the `challenge.yaml`, it will create a new challenge instead of updating it. This is likely due that GitLab CI doesn't store the `.ctf/config`.
- Better error handeling needs to be included so GitLab CI jobs don't always result in failed while challenges could be able to install/sync 